# Small-Internet Catalog

A "source of truth" that is really just a set of text files in a folder. Various outputs are possible by processing this stack of files.

## Content

- `content` folder and structure should be standalone and human readable. Ideally it should contain no metadata/dotfiles/configurations, only content.
- YAML header data is supported, most notably tags:

  ```
  ---
  tags: hardware,python,uxn
  ---
  ```

- Content can be plain text (.txt), HTML (.htm) or Markdown (.md).
- Small assets (icons, logos) might(?) be in this repo but ideally should be linked-in from an asset server

## Deployments

- To the extent possible, all processing should exist in self-contained in folders under `deploy.`

### Website (deploy/11ty)

Visible at: http://catalog.intrepidpixel.org  
Based on [barebones 11ty Example Site](https://gitlab.com/feralresearch/11ty-example-site), hosted on Gitlab Pages, auto-built on push to main branch by `.gitlab-ci.yaml`

```sh
# Run once. Installs dependencies
yarn install

# Local live preview so you can view/edit the markdown
# Should appear at:
yarn 11ty:dev

# Builds a website into /public.
# Usually handled by .gitlab-ci.yml, just here for debugging/convenience
yarn 11ty:deploy

```

### BBS

Coming soon?
