const { replaceAll } = require("lodash");
const removeMd = require("remove-markdown");

module.exports = (eleventyConfig) => {
  eleventyConfig.setUseGitIgnore(false);

  eleventyConfig.addPassthroughCopy({ "./_assets": "/" });

  eleventyConfig.addCollection("categoryList", (collectionApi) => {
    const categoryList = new Set();
    collectionApi.getAll().forEach((item) => {
      const category = item.filePathStem.split("/")[1];
      if (category != "category" && category != "index")
        categoryList.add(category);
    });
    return [...Array.from(categoryList)].sort((a, b) => a.localeCompare(b));
  });

  eleventyConfig.addCollection("articlesByCategory", (collectionApi) => {
    let articlesByCategory = {};
    collectionApi.getAll().forEach((item) => {
      const category = item.filePathStem.split("/")[1];
      if (category !== "category" && category !== "index") {
        if (!articlesByCategory[category]) articlesByCategory[category] = [];
        articlesByCategory[category].push({
          ...item.data.page,
          tags: item.data.tags,
          title: item.data.title
            ? item.data.title
            : item.data.page.fileSlug.replace(/-/g, " ")
        });
      }
    });
    return articlesByCategory;
  });

  eleventyConfig.addCollection("articles", (collectionApi) => {
    let articles = [];
    collectionApi.getAll().forEach((item) => {
      const category = item.filePathStem.split("/")[1];

      if (category !== "category" && category !== "index") {
        articles.push({
          ...item.data.page,
          content: item.template.inputContent,
          tags: item.data.tags,
          title: item.data.title
            ? item.data.title
            : item.data.page.fileSlug.replace(/-/g, " ")
        });
      }
    });
    return articles;
  });

  eleventyConfig.addCollection("tagsList", (collectionApi) => {
    const tagsSet = new Set();
    collectionApi.getAll().forEach((item) => {
      if (!item.data.tags) return;
      item.data.tags
        .toString()
        .split(",")
        .forEach((tag) => tagsSet.add(tag.trim()));
    });
    return [...Array.from(tagsSet)].sort((a, b) => a.localeCompare(b));
  });

  eleventyConfig.addCollection("articlesByTag", (collectionApi) => {
    let articlesByTag = {};
    collectionApi.getAll().forEach((item) => {
      if (!item.data.tags) return;
      item.data.tags
        .toString()
        .split(",")
        .forEach((tag) => {
          tag = tag.trim();
          if (!articlesByTag[tag]) articlesByTag[tag] = [];
          articlesByTag[tag].push({
            ...item.data.page,
            title: item.data.title
              ? item.data.title
              : item.data.page.fileSlug.replaceAll("-", " ")
          });
        });
    });
    return articlesByTag;
  });

  eleventyConfig.addShortcode("excerpt", (content) => {
    const text = removeMd(content)
      .replace(/\n|\r/g, " ")
      .replace(/http.*/g, "")
      .replace(/[^a-zA-Z ]/g, "")
      .replace(/\s\s+/g, " ");
    return [...new Set(text.toLowerCase().split(" "))].join(" ");
  });

  return {
    dir: {
      layouts: "../deploy/11ty/_layouts",
      input: "../../content",
      output: "../../public"
    },
    templateFormats: ["md", "njk", "html", "liquid"]
  };
};
