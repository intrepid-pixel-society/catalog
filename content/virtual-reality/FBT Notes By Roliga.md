By Roliga

We have a few people here who are interested in getting into full-body-tracking, so here’s some information on that. Let me know if I missed something important or have something to add.

There’s currently 3 options available for full-body tracking: Light-house based, IMU based, and camera based.

Light-house based tracking is the same tech used for tracking headsets like the Valve Index and Vive headsets. It uses base stations mounted on your walls in combination with tracking pucks you put on your body to track your movement.

It’s the most accurate type of tracking, but also the most expensive. For a full setup you’ll need:

-   At least 2 base stations, ideally 3: ~€160 * 2 = €320
-   3 tracking pucks: ~€130 * 3 = €390
-   Set of straps to attach those pucks to your body: ~€30

That’s a total of €740 for the absolute minimal setup. Now you might be able to get some gear second hand to shave some of that off, and if you already have a Valve Index or Vive headset you already have the base stations, but it still adds up. As for the hardware that’s available for this, everything has a couple of different versions:

Vive 1.0 tracking pucks

-   Don’t get these, they’re outdated and won’t work with newer lighthouse versions. Vive 2.0/2018 tracking pucks
-   [https://www.vive.com/media/filer_public/a9/4b/a94bc3c6-3c77-47ec-a8b7-849f7350edce/vive-tracker_2018_listing_resized.png](https://www.vive.com/media/filer_public/a9/4b/a94bc3c6-3c77-47ec-a8b7-849f7350edce/vive-tracker_2018_listing_resized.png)
-   [https://www.amazon.de/-/en/HTC-Vive-Tracker-2-0-Tracking/dp/B0748ZY323](https://www.amazon.de/-/en/HTC-Vive-Tracker-2-0-Tracking/dp/B0748ZY323)
-   [https://www.proshop.no/VR-hodesett/HTC-VIVE-Tracker-2018-Edition/2658271](https://www.proshop.no/VR-hodesett/HTC-VIVE-Tracker-2018-Edition/2658271)
-   Little bit bulky compared to other options
-   Only ~4h battery life, so extra battery packs may be necessary for longer play sessions
-   These can sometimes be gotten second hand for cheaper Vive 3.0 trackers
-   [https://business.vive.com/media/filer_public/enterprise/enterprise-tracker-pdp/vive-tracker-3_580x300.png](https://business.vive.com/media/filer_public/enterprise/enterprise-tracker-pdp/vive-tracker-3_580x300.png)
-   [https://business.vive.com/eu/product/vive-tracker/](https://business.vive.com/eu/product/vive-tracker/)
-   [https://unboundvr.eu/htc-vive-tracker-3-0](https://unboundvr.eu/htc-vive-tracker-3-0)
-   [https://www.amazon.de/-/en/99HASS002-00-HTC-Vive-Tracker-3-0-No-colour/dp/B08YY215VB](https://www.amazon.de/-/en/99HASS002-00-HTC-Vive-Tracker-3-0-No-colour/dp/B08YY215VB)
-   [https://www.proshop.no/VR-hodesett/HTC-Vive-Tracker-30/2930436](https://www.proshop.no/VR-hodesett/HTC-Vive-Tracker-30/2930436)
-   Small and light
-   Good battery life (~7h) Tundra tracker
-   [https://fabcross.jp/news/2021/dmln5300000p3n48-img/dmln5300000p3n4x.jpg](https://fabcross.jp/news/2021/dmln5300000p3n48-img/dmln5300000p3n4x.jpg)
-   [https://unboundvr.eu/tundra-x3-bundel](https://unboundvr.eu/tundra-x3-bundel)
-   Cheapest option if bought in a bundle
-   Bundle comes with basic straps
-   Smallest and lightest out of the bunch
-   Also good battery life (~7h)
-   Currently out of stock

Note that all tracker variants are essentially the same in terms of tracking quality. Image Image Image Vive 1.0 base stations

-   [https://www.gelecekevimde.com/Uploads/UrunResimleri/htc-vive-base-station-1.0-cd0b.jpg](https://www.gelecekevimde.com/Uploads/UrunResimleri/htc-vive-base-station-1.0-cd0b.jpg)
-   [https://www.vive.com/eu/accessory/base-station/](https://www.vive.com/eu/accessory/base-station/)
-   Can be gotten relatively cheap second-hand
-   More narrow tracking space, may be tricky to get good coverage in a small play space
-   Requires line-of-sight to other base stations, so may not be appropriate for some play space layouts
-   Can only use 2 in the same play space, so might be easy to occlude trackers Vive 2.0 base stations [https://www.droidshop.vn/wp-content/uploads/2018/12/vive-base-station-20-for-vive-pro-2.jpg](https://www.droidshop.vn/wp-content/uploads/2018/12/vive-base-station-20-for-vive-pro-2.jpg)
-   [https://www.vive.com/eu/accessory/base-station2/](https://www.vive.com/eu/accessory/base-station2/)
-   [https://www.proshop.no/VR-hodesett/HTC-VIVE-Base-Station-20/2791868](https://www.proshop.no/VR-hodesett/HTC-VIVE-Base-Station-20/2791868)
-   [https://www.komplett.no/product/1112385/gaming/gaming-utstyr/vr/vr-tilbehoer/htc-vive-base-station-20](https://www.komplett.no/product/1112385/gaming/gaming-utstyr/vr/vr-tilbehoer/htc-vive-base-station-20)
-   (Note the curved front VS the flat one on the 1.0)
-   Wider field of view than 1.0s
-   Technically more accurate, not sure if really noticable in practice tho
-   Can use 4 in the same play space for better coverage
-   Doesn’t come with wall-mounting hardware, so that’ll add a bit to the price. They have standard 1/4 20 camera mount screw holes on the back and bottom though, so any mounting hardware compatible with that should work. Note that speaker wall mounts often use this type of screw as well, so that may be a good option. Valve 2.0 base stations
-   [https://store.steampowered.com/app/1059570/Valve_Index_Base_Station/](https://store.steampowered.com/app/1059570/Valve_Index_Base_Station/)
-   Base station itself is exactly the same as the vive one.
-   Comes with mounting hardware and longer cable
-   Is slightly cheaper
-   Isn’t sold in every country Image Image If you already have lighthouse base stations for your VR headset, definitely go with this option. You get what you pay for in terms of quality and it’s easy to set up, basically plug-and-play.

If you have a Quest or some other non-lighthouse based headset, the choise is a bit harder. Base stations are expensive, and you need to run a calibration software before each play session to line up playspaces, and tracking may drift slightly over long sessions. IMU Based Tracking is a more recent tech that has some pros and cons. It’s a lot cheaper than lighthouse based tracking, and it doesn’t require your trackers to be visible to any base stations (in fact, there are no base stations).

The downside is that it drifts over time so needs recalibration during gameplay, and the tracking is less accurate than lighthouse based tracking. Tracking may have a bit of latency, and may feel “floaty” or “springy”.

SlimeVR

-   [https://www.crowdsupply.com/slimevr/slimevr-full-body-tracker](https://www.crowdsupply.com/slimevr/slimevr-full-body-tracker)
-   Either DIY build-it-yourself or can be gotten as a pre-built kit
-   A little complicated to use
-   Costs ~$165 to $235 HaritoraX
-   [https://en.shiftall.net/products/haritorax](https://en.shiftall.net/products/haritorax)
-   Supposedly slightly more accurate than SlimeVR
-   Easier to use and set up
-   Costs $270

Both those options are only up for pre-order currently, and it’s worth to keep in mind that they aren’t very battle-tested since not a lot of people have their hands on them.

Camera Based Tracking is the cheapest out of the bunch, and you get what you pay for. It’s really jank, depends a lot on your room lighting and colors etc.

It’s by far the worst tracking quality, and mosly only works while you’re standing up, and requires quite a bit of software tweaking to work right.

However all you need for this is though is a Kinect, ideally the newer “V2” version, and an adapter to connect it to your computer. While you can buy these new, they can be gotten for really cheap second-hand, and seeing as this is the extreme budget option, cheap secon-hand is your best bet.