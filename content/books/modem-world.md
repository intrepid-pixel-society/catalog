---
tags: book,modem,bbs
title: Modem World by Kevin Driscoll
---

# Modem World

If you haven’t picked up Kevin’s book yet, I highly recommend it: https://yalebooks.yale.edu/book/9780300248142/the-modem-world/
