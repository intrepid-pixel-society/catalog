require("dotenv").config();

module.exports = {
  env: { baseHref: process.env.BASE_HREF },
  permalink: (data) => {
    return `${data.page.filePathStem}.html`;
  },
  layout: "page.njk"
};
