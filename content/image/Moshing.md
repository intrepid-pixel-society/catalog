C64 Fonts [https://style64.org/release/c64-truetype-v1.2.1-style](https://style64.org/release/c64-truetype-v1.2.1-style)

1 Bit Images

-   [Dither Me This](https://doodad.dev/dither-me-this/)
-   [Ditherlicious 1bit Image Crusher](https://29a.ch/ditherlicious/)

Atkinson Dithering [https://beyondloom.com/blog/dither.html](https://beyondloom.com/blog/dither.html) [https://en.wikipedia.org/wiki/Dither](https://en.wikipedia.org/wiki/Dither)

Moshpit

-   [Photomosh](https://photomosh.com/)