# Scenius

"There’s a healthier way of thinking about creativity that the musician Brian Eno refers to as “scenius.” Under this model, great ideas are often birthed by a group of creative individuals—artists, curators, thinkers, theorists, and other tastemakers—who make up an “ecology of talent.” If you look back closely at history, many of the people who we think of as lone geniuses were actually part of “a whole scene of people who were supporting each other, looking at each other’s work, copying from each other, stealing ideas, and contributing ideas.” Scenius doesn’t take away from the achievements of those great individuals: it just acknowledges that good work isn’t created in a vacuum, and that creativity is always, in some sense, a collaboration, the result of a mind connected to other minds."

https://austinkleon.com/2017/05/12/scenius/
