# Category

<ul>
{%- for category in collections.categoryList -%}
  <li>{{ category }}</li>
  <ul>
    {%- for article in collections.articlesByCategory[category] -%}
    <li><a href="{{ article.url }}">{{article.title}}</a></li>
    {%- endfor -%}
  </ul>
{%- endfor -%}
</ul>
