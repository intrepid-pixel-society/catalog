---
tags: hardware, eink
---


Eink Projects [https://www.engineersneedart.com/systemsix/systemsix.html](https://www.engineersneedart.com/systemsix/systemsix.html) [https://alexanderklopping.medium.com/an-updated-daily-front-page-of-the-new-york-times-as-artwork-on-your-wall-3b28c3261478](https://alexanderklopping.medium.com/an-updated-daily-front-page-of-the-new-york-times-as-artwork-on-your-wall-3b28c3261478)



# Inkplate

![A picture of an inkplate](https://assets.intrepidpixel.org/catalog/img/inkplate.jpeg)

![](https://assets.intrepidpixel.org/catalog/img/withtext.jpeg)

![](https://assets.intrepidpixel.org/catalog/img/prentler.jpeg)
