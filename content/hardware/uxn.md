# UXN

  

## Background

[THESE ARTISTS ARE MAKING TINY ROMS THAT WILL PROBABLY OUTLIVE US ALL](https://www.theverge.com/22935074/hundred-rabbits-uxn-roms-preservation)

  

## Intro / Tutorials

<https://wiki.xxiivv.com/site/varvara.html>

<https://wiki.xxiivv.com/site/uxn.html>

<https://100r.co/site/uxn.html>

<https://compudanzas.net/uxn_tutorial.html#instructions>

<https://eli.li/2021/09/27/how-to-install-uxn-on-macos>

<https://compudanzas.net/uxn_tutorial_day_2.html>

  

## Projects

<https://github.com/hundredrabbits/awesome-uxn>

<https://compudanzas.net/uxner%C3%ADa.html>

<https://wiki.xxiivv.com/site/games.html>

  

## Hardware

Playdate: <https://git.sr.ht/~rabbits/uxn-playdate>

Pico: <https://git.sr.ht/~alderwick/pico-uxn/log>

  

## On the Web

<https://metasyn.github.io/learn-uxn/>#

  

## Wiktopher

<https://wiktopher.ca/site/chapter_01.html>

<https://100r.co/site/wiktopher.html>

  

## Font Editor

<https://wiki.xxiivv.com/site/turye.html>

  

## Presentation

<https://wiki.xxiivv.com/site/adelie.html>

<https://git.sr.ht/~rabbits/adelie>

  
  

## C64 Font Set

<http://fontpro.com/commodore-64-font-623>

<https://style64.org/c64-truetype>

  

## Catclock

<https://wiki.xxiivv.com/site/catclock.html>