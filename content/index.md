# Small-Internet Catalog

Last Update: {{ page.date }}

In the future the internet will be smaller, more localized and less wasteful. It won't be measured in advertising dollars or click metrics, but rather by its utility to community.

This site is an ongoing project (2022-?), consisting of an annotated collection of resources related to this vision of the future compiled by [Andrew](https://feralresearch.org/) and [Jesse](https://jklabs.net/). We're fans of: degrowth, decentralization, minimal computing, human scale, legitimate peripheral participation, user-centered design, anti-capitalism and moving slowly.

To update or make changes to the catalog, get in touch or [make a pull request to this repository](https://gitlab.com/intrepid-pixel-society/catalog).

![A picture of the internet](https://assets.intrepidpixel.org/catalog/img/arpanet_oct_1980.gif)

<h2>Category</h2>
<ul>
{%- for category in collections.categoryList -%}
  <li>{{ category }}</li>
  <ul>
    {%- for article in collections.articlesByCategory[category] -%}
    <li><a href="{{ article.url }}">{{article.title}}</a></li>
    {%- endfor -%}
  </ul>
{%- endfor -%}
</ul>
