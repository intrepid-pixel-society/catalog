# Shinobi Website

https://shinobi.website/index.txt

"A shinobi website is a text-based, RSS focused blogging "system". I put
the word system in quotes since it's really just a simple bash script
that converts plain text files into an RSS feed. So, it isn't an actual
blogging platform or website in the traditional sense."
